<a href="app/aksi_reset_all" onclick="javasciprt: return confirm('Are You Sure ?')" class="btn btn-danger">RESET ALL USER</a><br><br>
<table class="table table-bordered tabel-data" style="margin-bottom: 10px">
            <thead>
            <tr>
                <th>No</th>
				<th>Nama Siswa</th>
				
				<th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $start = 0;
            $siswa_data= $this->db->get_where('user',array('akses'=>'siswa'))->result();
            foreach ($siswa_data as $siswa)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $siswa->nama_lengkap ?></td>
			<td style="text-align:center" width="200px">
				<a href="app/aksi_reset/<?php echo $siswa->user_id ?>" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm('Are You Sure ?')">RESET ALL</a>

                <?php 
                foreach ($this->db->get('batch')->result() as $dt) {
                    $cek = $this->db->get_where('akses_batch', array('user_id'=>$siswa->user_id,'batch_id'=>$dt->batch_id));
                    if ($cek->num_rows() > 0) {
                        $btn = 'btn btn-success btn-xs';
                        $link = 'app/aksi_reset_batch/'.$dt->batch_id.'/'.$siswa->user_id;
                    } else {
                        $btn = 'btn btn-danger btn-xs';
                        $link = '#';
                    }
                 ?>

                <a href="<?php echo $link ?>" class="<?php echo $btn ?>" onclick="javasciprt: return confirm('Are You Sure ?')"><?php echo $dt->nama_batch ?> </a>

            <?php } ?>
			</td>
		</tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        